package com.edgenda.bnc.skillsmanager.rest.ressource;

import java.util.List;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.hateoas.ResourceSupport;

import com.edgenda.bnc.skillsmanager.model.Skill;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class EmployeeResource extends ResourceSupport {

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    @Email
    @NotEmpty
    private String email;

    @JsonIgnoreProperties({"employees"})
    private List<Skill> skills;

    public EmployeeResource(String firstName, String lastName, String email, List<Skill> skills) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.skills = skills;
    }

    public EmployeeResource() {
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

}
