package com.edgenda.bnc.skillsmanager;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.web.bind.annotation.*;

import com.edgenda.bnc.skillsmanager.model.Employee;
import com.edgenda.bnc.skillsmanager.model.Skill;
import com.edgenda.bnc.skillsmanager.service.SkillService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/skills")
public class SkillController {

	private final SkillService skillService;
	
	@Autowired
	public SkillController (SkillService skillService) {
		this.skillService = skillService;
	}

	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public Skill getSkill(@PathVariable Long id) {
		return skillService.getSkill(id);
	}

	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(method = RequestMethod.GET)
	public List<Skill> getSkills() {
		return skillService.getSkills();
	}

	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(path = "/{id}/employees", method = RequestMethod.GET)
	public List<Employee> getEmployeesWithSkill(@PathVariable Long skillId) {
		return skillService.getEmployeesWithSkill(skillId);
	}
}
