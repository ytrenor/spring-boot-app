package com.edgenda.bnc.skillsmanager;

import java.util.List;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.edgenda.bnc.skillsmanager.model.Employee;
import com.edgenda.bnc.skillsmanager.model.Skill;
import com.edgenda.bnc.skillsmanager.rest.ressource.EmployeeResource;
import com.edgenda.bnc.skillsmanager.service.EmployeeService;

import io.swagger.annotations.ApiOperation;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

	private final EmployeeService employeeService;
	
	@Autowired
	public EmployeeController (EmployeeService employeeService) {
		this.employeeService = employeeService;
	}
	
	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(path = "/{id}", method = RequestMethod.GET)
	public EmployeeResource getEmployee(@PathVariable Long id) {
		Employee employee = employeeService.getEmployee(id);
		
		EmployeeResource employeeResource = new EmployeeResource(employee.getFirstName(), employee.getLastName(), employee.getEmail(), employee.getSkills());
		employeeResource.add(
				linkTo(methodOn(EmployeeController.class).getEmployee(id)).withSelfRel(),
				linkTo(methodOn(EmployeeController.class).getEmployeeSkills(id)).withRel("links"));
		return employeeResource;
//		return employeeService.getEmployee(id);
	}
	
	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(method = RequestMethod.GET)
	public List<Employee> getEmployees() {
		return employeeService.getEmployees();
	}
	
	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public Employee createEmployee(@RequestBody Employee employee) {
		return employeeService.createEmployee(employee);
	}

	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(method = RequestMethod.PUT)
	public void updateEmployee(@RequestBody Employee employee) {
		employeeService.updateEmployee(employee);
	}

	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
	public void deleteEmployee(@PathVariable Long id) {
		employeeService.deleteEmployee(id);
	}

	@ApiOperation(value="${operation1.summary}", notes="Ceci est la description")
	@RequestMapping(path = "/{id}/skills", method = RequestMethod.GET)
    public List<Skill> getEmployeeSkills(@PathVariable Long employeeId) {
        return employeeService.getEmployeeSkills(employeeId);
    }
}
